variable "ami" {
  type        = string
  description = "Windows_Server-2022-English-Full-Base-2023.08.10"
  default     = "ami-09301a37d119fe4c5"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "My EC2 Instance"
}